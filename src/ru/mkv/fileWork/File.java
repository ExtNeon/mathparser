package ru.mkv.fileWork;

import java.io.*;

/**
 * Данный класс представляет файл, как объект, что позволяет полноценно работать с ним на уровне ООП.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class File {
    private String fileName;
    private int lineCounterPos;

    public File(String fileName) {
        this.fileName = fileName;
        this.resetLinePosition();
    }

    /**
     * Метод сбрасывает счётчик строк для метода @code{readln}.
     */
    public void resetLinePosition() {
        this.lineCounterPos = 0;
    }

    /**
     * Данный метод записывает текст @code{text} в файл
     * @param text входящий текст
     * @throws IOException
     */
    public void write(String text) throws IOException {
        if (text.length() == 0) {
            return; //Не будем выбрасывать исключение. Пока что...
            //Всё же, оно тут нужно, так как вносит ясность, если мы там что - то натворили, и входящая строка оказалась пустой.
        }
        FileWriter fileWriter = new FileWriter(fileName);
        /*if (index < 0) {
            index = 0;
        }*/
        fileWriter.write(text, 0, text.length());
        fileWriter.close();
    }

    /**
     * Метод читает из файла строку длины @code{length}, начиная с индекса @code{index}.
     * @param index Индекс читаемой строки
     * @param length Длина читаемой строки
     * @return Строка длиной @code{length}, прочитанная из файла по индексу code{index}.
     * @throws IOException
     */
    public String read(int index, int length) throws IOException {
        if (index < 0) {
            throw new IllegalArgumentException("Index is lower, than zero");
        }
        if (length <= 0) {
            throw new IllegalArgumentException("Read pattern length cannot be lower or equal zero");
        }
        FileReader fileReader = new FileReader(fileName);
        char[] buf = new char[length];
        fileReader.read(buf,index,length);
        fileReader.close();
        return charArrayToString(buf);
    }

    /**
     * Метод считывает данные из файла, начиная с индекса @code{index}.
     * @param index Индекс начала считывания данных
     * @return Строка, содержащая данные из файла по индексу @code{index}, и до конца.
     * @throws IOException
     */
    public String read(int index) throws IOException {
        if (index < 0) {
            throw new IllegalArgumentException("Index is lower, than zero");
        }
        FileReader fileReader = new FileReader(fileName);
        int size = this.size();
        char[] buf = new char[size];
        fileReader.read(buf,index,size);
        fileReader.close();
        return charArrayToString(buf);
    }

    /**
     * Возвращает содержимое файла.
     * @return Содержимое файла.
     * @throws IOException
     */
    public String read() throws IOException {
        FileReader fileReader = new FileReader(fileName);
        int size = this.size();
        char[] buf = new char[size];
        fileReader.read(buf,0,size);
        fileReader.close();
        return charArrayToString(buf);
    }

    /**
     * Возвращает размер файла в байтах(?).
     * @return Размер файла в байтах.
     * @throws IOException
     */
    public int size() throws IOException {
        FileReader fileReader = new FileReader(fileName);
        int i = 0;
        while (fileReader.read() != -1) {
            i++;
        }
        fileReader.close();
        return i; //Медленно, наверное!
    }

    /**
     * Метод конвертирует массив символов в строку.
     * @param charArray Входящий массив символов
     * @return Строка, состоящая из последовательности символов code{charArray}.
     */
    private static String charArrayToString(char[] charArray) {
        if (charArray.length == 0) {
            return "";
        }
        StringBuffer result = new StringBuffer();
        result.append(charArray);
        return result.toString();
    }

    /**
     * Считывает из файла и возвращает определённую строку с номером @code{lineNum}.
     * @param lineNum Номер считываемой строки. Нумерация строк начинается с нуля.
     * @return Строка с номером @code{lineNum}. Если такой строки нет в файле, то возвращает @code{null}.
     * @throws IOException
     */
    public String readln(int lineNum) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName));
        for (int i = 0; i <= lineNum; i++) {
            String result = bufferedReader.readLine();
            if (i == lineNum) {
                bufferedReader.close();
                return result;
            }
        }
        bufferedReader.close();
        return null;
    }

    /**
     * Возвращает очередную считанную строку из файла.
     *
     * Механика работы данного метода такая же, как и одноимённого метода класса BufferedReader.
     * Каждый новый вызов данного метода увеличивает внутренний счётчик строк, позволяя построчно считать файл в цикле.
     * Пример
     * ----Содержимое файла----
     * 1234
     * abcd
     * xyz
     * ------Конец файла-------
     * При первом вызове данного метода, он вернёт "1234". При следующем "abcd". И так далее, пока файл не кончится.
     * Сбросить счётчик на ноль можно методом @code{resetLinePosition}. После этого, можно будет снова читать файл с нуля.
     * @return Очередная строка, считанная из файла, либо null, если был достигнут конец файла.
     * @throws IOException
     */
    public String readln() throws IOException {
        return readln(lineCounterPos++);
    }

    //На данный момент, данный метод неработоспособен.
    // TODO: 30.03.2017 НАДО ЗАКРЫВАТЬ ЗА СОБОЙ ФАЙЛ
    /*private*/ public int getLineStartIndex(int lineNum) throws IOException {
        if (lineNum == 0) {
            return 0;
        }
        if (lineNum < 0) {
            throw new IllegalArgumentException("Line num cannot be lower than zero");
        }
        FileReader fileReader = new FileReader(fileName);
        int stIndex = 0;
        int counter = 0;
        int position = 0;
        while (true) {
            char readedChar = ' ';
            int tempSmb = fileReader.read();
            if (tempSmb != -1) {
                readedChar = (char)tempSmb;
            } else {
                fileReader.close();
                return -1;
            }
            position++;
            if (readedChar == '\n') {
                if (counter == lineNum) {
                    return stIndex;
                } else {
                    counter++;
                    stIndex = position;
                }
            }
        }
    }
}
