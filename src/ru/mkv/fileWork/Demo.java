package ru.mkv.fileWork;

import ru.mkv.parser.FractionParser;

import java.io.IOException;

/**
 * Класс, демонстрирующий работу с классом File, частично использующий класс FractionParser.
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class Demo {
    public static void main(String args[]) throws IOException {
        File file = new File("input.txt");
        String result = "";
        try {
            String input;
            while ((input = file.readln()) != null) {
                result += input + " = ";
                try {
                    result += FractionParser.parse(input) + "\n";
                } catch (Exception e) {
                    result += "Невозможно вычислить\n";
                }
            }
            new File("output.txt").write(result);
        } catch (Exception e) {
            System.err.println("У нас проблема: " + e);
        }
    }
}
