package ru.mkv.parser;

import java.util.Scanner;

/**
 * Демонстрацонный класс, демонстрирующий возможности класса FractionParser.
 * @author Малякин К.В.
 */
public class Demo {
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Fraction f1 = new Fraction(3,10);
        Fraction f2 = new Fraction(2,6);
        System.out.println(f1.toString() + " + " + f2.toString() + " = " + FractionOperations.add(f1,f2).toString());
        System.out.print("Enter a expression: ");
        String sss = "";
        while (true) {
            try {
                sss =scanner.nextLine();
                System.out.print(FractionParser.parse(sss) + "\nEnter an expression: ");
            } catch (IllegalArgumentException e) {
                //NOTHING
                System.err.println("Captain, we are drowning!");
                System.err.print("Sir, you ordered \""+sss+"\", but something was happened: \n");
                e.printStackTrace();
            }
        }
    }
}
