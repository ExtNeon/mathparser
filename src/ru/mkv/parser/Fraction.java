package ru.mkv.parser;

/**
 * Класс, описывающий дробь
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class Fraction {
    int numerator;
    int denominator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        if (denominator == 0) {
            throw new ArithmeticException("Denominator equals 0");
        }
        this.denominator = denominator;
    }

    public Fraction() {
        this(1,1);
    }

    public int getNumerator() {
        return numerator;
    }

    public void setNumerator(int numerator) {
        this.numerator = numerator;
    }

    public int getDenominator() {
        return denominator;
    }

    public void setDenominator(int denominator) {
        if (denominator == 0) {
            throw new ArithmeticException("Denominator equals 0");
        }
        this.denominator = denominator;
    }

    public boolean equals(Fraction fraction) {
        return fraction.numerator == this.numerator && fraction.denominator == this.denominator;
    }

    public double toDecimal() {
        return numerator / denominator;
    }

    @Override
    public String toString() {
        if (denominator == 1) {
            return numerator + "";
        } else {
            return numerator + "/" + denominator;
        }
    }

    public void valueOf(String input) {
        if (input.indexOf('/') == -1) {
            try {
                this.numerator = Integer.valueOf(input);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Number expected, but \"" + input + "\" found in nominator part");
            }
            this.denominator = 1;
            return;
        }
        String denom = input.substring(input.indexOf('/')+1);
        try {
            this.numerator = Integer.parseInt(input.substring(0, input.indexOf('/')));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Number expected, but \"" + input.substring(0, input.indexOf('/')) + "\" found in nominator part");
        }
        try {
            this.setDenominator(Integer.parseInt(denom));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Number expected, but \"" + denom + "\" found in denominator part");
        }
    }
}
