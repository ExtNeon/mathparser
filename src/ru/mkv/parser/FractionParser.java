package ru.mkv.parser;

/**
 * Класс содержит статичные методы, предназначенные для разбора и решения различных арифметических выражений с обычными дробями.
 * Основным методом является parse. Он поддерживает довольно сложные выражения, с использованием вложенных и параллельных
 * скобок, а также с отрицательными операндами.
 * @author Малякин Кирилл. 15ОИТ20.
 */
public class FractionParser {

    final static String STATEMENTS = ":*-+";

    /**
     * Метод, вычисляющий результат выражения {@code expression}.
     *
     * Имеет любопытные исключения, а именно, показывает в каком именно месте выражения он обнаружил ошибку.
     * @param expression Входное выражение
     * @return Результат вычислений
     */
    public static String parse(String expression) {
        // 1/2 - 7/6 * (3/5 + 6/10) - ((1/4 + 1/2) + 3/2) - решает!
        /*
            Алгоритм работы метода:
          1) Полностью удаляем пробелы.
          2) Обработка скобок. Находим открывающую скобку, ищем следующую за ней закрывающую. Рекурсивно решаем содержимое.
          3) Соответствующим методом заменяем минус в начале строки  на m, находим [statement]-, тоже меняем на [statement]m.
          4) Начинаем итеративно искать операторы в строке, начиная с самого приоритетного.
             Как только находим, начинаем идти в обратную сторону, пока не найдём очередной оператор, или не упрёмся
             в начало строки - это индекс a.
             Так же делаем в другую сторону - это индекс с.
             Индекс b - это индекс самого оператора.
             a < b < c
          5) Вырезаем кусок с a по с. Это выражение. Сохраняем в соответствующей переменной.
          6) Делим получившееся выражение на две части. Первый операнд, второй операнд. В спец. переменную выносим оператор.
          7) Вызываем метод calculate, который выполняет вычисление бинарной операции, вставляем результат
             в основную строку по индексу а.
          8) Продолжаем, пока в строке остаётся хоть один оператор.
          9) Возвращаем полученный резуьтат, заменяя при этом все символы m на минусы.
         */
        while (expression.indexOf(' ') >= 0) {
            int i = expression.indexOf(' ');
            expression = StringTools.delete(expression,i,i);
        }
        if (expression.length() == 0) {
            //throw new IllegalArgumentException("Empty expression");
            return ""; //Не будем делать из этого исключение
        }
        int openBranchIndex = -1;
        for (int i = 0; i < expression.length(); i++) {
            char tempChar = expression.charAt(i);
            if (tempChar == '(') {
                openBranchIndex = i;
            }
            if (tempChar == ')') {
                if (openBranchIndex == -1) {
                    throw new IllegalArgumentException("Expression error: unexpected token. " +
                            "Unexpected completion of the internal expression at [" + i + "]\n" + pointToTheSymbol(expression, i)) ;
                }
                String internalExpression = StringTools.cut(expression, openBranchIndex + 1, i - 1);
                String newExpression = StringTools.delete(expression, openBranchIndex, i);
                try {
                    newExpression = StringTools.insert(newExpression, parse(internalExpression), openBranchIndex - 1);
                } catch (IllegalArgumentException e) {
                    throw new IllegalArgumentException(e + "\nAt [" + openBranchIndex + "]:\n" + pointToTheSymbol(expression, openBranchIndex)) ;
                }
                expression = newExpression;
                openBranchIndex = -1;
                i = -1;
            }
            if (i == expression.length() - 1 & openBranchIndex != -1) {
                throw new IllegalArgumentException("Expression error: ')' expected. " +
                        "Unterminated opening bracket at [" + openBranchIndex + "]\n" + pointToTheSymbol(expression, openBranchIndex));
            }
        }
        //Работаем с выражением
        //Сначала объявляем все необходимые переменные
        char statement;
        String firstParam,secondParam;
        int firstIndex, lastIndex, statementIndex;
        boolean indexFounded;
        expression = formatNegativeExpressionFragments(expression, STATEMENTS); // Форматируем минус
        for (int currStatement = 0; currStatement < STATEMENTS.length(); currStatement++) {
            statement = STATEMENTS.charAt(currStatement);
            expression = formatNegativeExpressionFragments(expression, STATEMENTS);
            while (expression.indexOf(statement) >= 0) {
                boolean incorrectExpression = false;
                //Сканим выражение.
                statementIndex = expression.indexOf(statement);
                //Как только нашли индекс оператора, начинаем искать индекс предыдущего оператора и следующего оператора.
                lastIndex = expression.length() - 1;
                firstIndex = 0; //Задаём по умолчанию
                indexFounded = false;
                for (int i = statementIndex - 1; i >= 0 & !indexFounded; i--) { //Сначала идём назад
                    for (int j = 0; j < STATEMENTS.length() & !indexFounded; j++) {
                        char a = expression.charAt(i);
                        char b = STATEMENTS.charAt(j);
                        if (a == b & i > firstIndex) {
                            indexFounded = true;
                            firstIndex = i+1;
                        }
                    }
                }
                indexFounded = false;
                for (int i = statementIndex + 1; i < expression.length() & !indexFounded; i++) { //Потом вперёд
                    for (int j = 0; j < STATEMENTS.length() & !indexFounded; j++) {
                        if (expression.charAt(i) == STATEMENTS.charAt(j) & i < lastIndex) {
                            indexFounded = true;
                            lastIndex = i-1;
                        }
                    }
                }
                //Громоздкая проверка на некорректность выражения. Костыль костылём, как ни посмотри.
                if (lastIndex == expression.length() - 1) {
                    for (int i = 0; i < STATEMENTS.length() & !incorrectExpression; i++) {
                        incorrectExpression = STATEMENTS.charAt(i) == expression.charAt(lastIndex);
                    }
                }
                if (firstIndex == statementIndex | lastIndex == statementIndex | statementIndex == 0 | //Тут был кусок, но я временно его вырезал
                        incorrectExpression /*| ((lastIndex != expression.length() - 1) & lastIndex - statementIndex == 1)*/) {
/*                    if ((lastIndex != expression.length() - 1) & lastIndex - statementIndex == 1) { //КОСТЫЛЬ!
                        statementIndex++;
                    }*/
                    throw new IllegalArgumentException("Illegal expression: operand expected, but '" + expression.charAt(statementIndex) + "' found\n" + pointToTheSymbol(expression, statementIndex));
                }
                firstParam = StringTools.cut(expression, firstIndex, statementIndex-1);
                secondParam = StringTools.cut(expression, statementIndex+1, lastIndex);
                String lastExpr = expression;
                expression = StringTools.delete(expression, firstIndex, lastIndex);
                try {
                    expression = StringTools.insert(expression, calculate(firstParam, secondParam, statement), firstIndex - 1);///
                } catch (ArithmeticException e) {
                    throw new IllegalArgumentException(e + " while performing this operation: \n" + pointToTheSymbol(lastExpr, statementIndex));
                }
                expression = formatNegativeExpressionFragments(expression, STATEMENTS);
            }
        }
        return calculate(expression,"1",'*'); //Костыль.
    }

    /**
     * Метод, вычисляющий бинарные операции с обычными дробями.
     * @param n1 первый операнл
     * @param n2 второй операнд
     * @param statement оператор
     * @return результат вычислений.
     */
    private static String calculate(String n1, String n2, char statement) {
        Fraction f1 = new Fraction();
        Fraction f2 = new Fraction();
        f1.valueOf(n1.replace('m','-'));
        f2.valueOf(n2.replace('m','-'));
        switch (statement) {
            case ':': return FractionOperations.divide(f1,f2).toString();
            case '*': return FractionOperations.multiple(f1,f2).toString();
            case '-': return FractionOperations.substract(f1,f2).toString();
            case '+': return FractionOperations.add(f1,f2).toString();
        }
        return f1.toString();
    }

    /**
     * Форматирует отрицательные дроби в выражении, для последующей его обработки методом {@code parse}.
     * @param expression Исходное выражение
     * @param STATEMENTS Набор операторов
     * @return Отформатированное выражение
     */
    private static String formatNegativeExpressionFragments(String expression, final String STATEMENTS) {
        for (int i = 0; i < STATEMENTS.length(); i++) { //ПРЯМАЯ ЗАМЕНА МИНУСА НА M
            for (int j = expression.indexOf(STATEMENTS.charAt(i)); j >= 0 & j < expression.length()-1; j++ ) {
                if (expression.charAt(j) == STATEMENTS.charAt(i) &  expression.charAt(j+1) == '-'){
                    expression = StringTools.delete(expression, j+1, j+1);
                    expression = StringTools.insert(expression, "m", j);
                }
            }
        }
        if (expression.charAt(0) == '-') {
            expression = StringTools.delete(expression,0,0);
            expression = StringTools.insert(expression,"m",-1);
        }
        return expression;
    }

    /**
     * Метод предназначен для удобного указания места ошибки в выражении пользователя.
     * @param input Выражение
     * @param index Индекс места ошибки
     * @return Отформатированная строка
     */
    public static String pointToTheSymbol(String input, int index) {
        return input + "\n" + StringTools.genFilledString(index,' ')+ "^";
    }

    /*private static ErrorCode[] validate(String expression) {
        //Сначала проверяем скобки.
        ArrayList<ErrorCode> result = new ArrayList();
        int openingBrCount = 0;
        int closingBrCount = 0;
        for (int i = 0; i < expression.length(); i++) {
            switch (expression.charAt(i)) {
                case '(': openingBrCount++; break;
                case ')': closingBrCount++; break;
            }
        }
        if (openingBrCount != closingBrCount) {
            result.add(new ErrorCode(1,0));
            ddd
        }
    }*/

}
