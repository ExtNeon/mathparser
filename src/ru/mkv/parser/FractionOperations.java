package ru.mkv.parser;

/**
 * Класс содержит методы для работы с объектами типа Fraction
 * @author Малякин Кирилл. Гр. 15ОИТ20.
 */
public class FractionOperations {

    public static Fraction multiple(Fraction f1, Fraction f2) {
        Fraction result = new Fraction();
        result.numerator = f1.numerator * f2.numerator;
        result.denominator = f1.denominator * f2.denominator;
        reduce(result); //По умолчанию
        return result;
    }

    public static Fraction multiple(Fraction f1, int number) {
        Fraction result = new Fraction();
        result.numerator = f1.numerator * number;
        reduce(result); //По умолчанию
        return result;
    }

    public static void invert(Fraction fraction) {
        int temp = fraction.numerator;
        fraction.numerator = fraction.denominator;
        fraction.denominator = temp;
    }

    public static Fraction getInverted(Fraction fraction) {
        return new Fraction(fraction.denominator,fraction.numerator);
    }

    public static Fraction divide(Fraction f1, Fraction f2) {
        return multiple(f1,getInverted(f2));
    }

    public static int lowestСommonDenominator(int firstDivider, int secondDivider) {
        if (firstDivider == secondDivider) {
            return firstDivider;
        }
        for (int i = 1; ; i++) {
            if (i % firstDivider == 0 && i % secondDivider == 0) {
                return i;
            }
        }
    }

    public static int lowestСommonDenominator(Fraction f1, Fraction f2) {
        return lowestСommonDenominator(f1.denominator,f2.denominator);
    }

    /**
     * Метод, сокращающий и форматирующий дробь по правилам (отрицательная дробь - это отрицательный числитель).
     *
     * @param fraction Исходная дробь
     */
    public static void reduce(Fraction fraction) {
        if (fraction.denominator < 0) {
            fraction.denominator *= -1;
            fraction.numerator *= -1;
        }
        for (int i = 2; i <= fraction.denominator & i <= fraction.numerator; i++) {
            if (fraction.numerator % i == 0 && fraction.denominator % i == 0) {
                fraction.numerator /= i;
                fraction.denominator /= i;
                i = 1;
            }
        }
    }

    public static Fraction add(Fraction f1, Fraction f2) {
        Fraction result = new Fraction();
        result.denominator = lowestСommonDenominator(f1,f2);
        int num1 = (result.denominator / f1.denominator) * f1.numerator;
        int num2 = (result.denominator / f2.denominator) * f2.numerator;
        result.numerator = num1 + num2;
        reduce(result);
        return result;
    }

    public static Fraction substract(Fraction f1, Fraction f2) {
        Fraction result = new Fraction();
        result.denominator = lowestСommonDenominator(f1,f2);
        int num1 = (result.denominator / f1.denominator) * f1.numerator;
        int num2 = (result.denominator / f2.denominator) * f2.numerator;
        result.numerator = num1 - num2;
        reduce(result);
        return result;
    }
}
