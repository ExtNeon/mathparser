package ru.mkv.parser;

/**
 * Данный класс содержит статичные методы для работы с объектами класса String.
 * Некоторые методы в данном классе предоставляют альтернативу встроенным методам класса String
 * @author Малякин Кирилл. 15ОИТ20.
 */
public class StringTools {

    /**
     * Метод, вставляющий в строку target строку substring по индексу index
     * Пример: "Пост". Индекс = 1; Подстрока = "го"
     * Результат: "Погост"
     * То есть, вставка подстроки происходит после символа по индексу index
     * @param target Целевая строка
     * @param substring Подстрока
     * @param index Индекс
     */
    public static String insert(String target, final String substring, final int index) {
        //ИСПОЛЬЗУЕМ КОСТЫЛИЩЕ. Потому, что в джаве убогие инструменты для работы со строкой.
        StringBuffer firstPart = new StringBuffer(); //Стрингбафферы нужны для того, чтобы использовать меньше ресурсов.
        StringBuffer secondPart = new StringBuffer();
        if (index >= 0) {
            for (int i = 0; i <= index & i < target.length(); i++) {
                firstPart.append(target.charAt(i));
            }
        }
        for (int i = index + 1; i < target.length(); i++) {
            secondPart.append(target.charAt(i));
        }
        return firstPart + substring + secondPart;
    }

    /**
     * Метод, удаляющий из строки определённый фрагмент, начиная с индекса startIndex, до endIndex.
     * Символы, находящиеся по индексам, метод УДАЛЯЕТ.
     * Пример: target = "Безысходность"; startIndex = 2; endIndex = 6;
     * Результат: target = "Бедность"
     * @param target Целевая строка
     * @param startIndex Начальный индекс удаляемого фрагмента
     * @param endIndex Конечный индекс удаляемого фрагмента
     */
    public static String delete(String target, final int startIndex, final int endIndex) {
        //ИСПОЛЬЗУЕМ КОСТЫЛИЩЕ. Потому, что в джаве убогие инструменты для работы со строкой.
        StringBuffer firstPart = new StringBuffer(); //Стрингбафферы нужны для того, чтобы использовать меньше ресурсов.
        StringBuffer secondPart = new StringBuffer();
        for (int i = 0; i < startIndex; i++) {
            firstPart.append(target.charAt(i));
        }
        if (endIndex < target.length()) {
            for (int i = endIndex + 1; i < target.length(); i++) {
                secondPart.append(target.charAt(i));
            }
        }
        return firstPart.toString() + secondPart;
    }

    /**
     * Метод вырезает из строки определённый фрагмент, начиная с индекса startIndex, до endIndex.
     * Причина его существования - убогие встроенные строковые инструменты.
     * Пример: input = "Сумашествие"; startIndex = 4; endIndex = 7;
     * Результат: "шест"
     * @param input
     * @param startIndex
     * @param endIndex
     * @return
     */
    public static String cut(String input, final int startIndex, final int endIndex) {
        StringBuffer result = new StringBuffer();
        if (startIndex >= input.length() | startIndex < 0) {
            throw new IllegalArgumentException("startIndex was out from borders of string");
        }
        /*if (endIndex >= input.length() | endIndex < startIndex) {
            throw new IllegalArgumentException("endIndex was out from borders of string");
        }*/
        /*if (startIndex == endIndex) {
            throw new IllegalArgumentException("startIndex equals endIndex");
        }*/
        for (int i = startIndex; i <= endIndex; i++) {
            result.append(input.charAt(i));
        }
        return result.toString();
    }


    public static int firstCharPos(String input, int startIndex, char req) {
        if (input.length() == 0) {
            return -1; //No exception throwing
        }
        if (startIndex >= input.length() | startIndex < 0) {
            throw new IllegalArgumentException("Index was out from borders of string");
        }
        for (int i = startIndex; i < input.length(); i++) {
            if (input.charAt(i) == req) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Метод находит индекс последнего вхождения символа в строке
     * @param input
     * @param endIndex - крайняя граница для индекса
     * @param req
     * @return
     */
    public static int lastCharPos(String input, int endIndex, char req) {
        if (input.length() == 0) {
            return -1; //No exception throwing
        }
        if (endIndex >= input.length() | endIndex < 0) {
            throw new IllegalArgumentException("Index was out from borders of string");
        }
        for (int i = endIndex; i >= 0; i--) {
            if (input.charAt(i) == req) {
                return i;
            }
        }
        return -1;
    }

    public static int charPos(String input, int index, int endIndex, char req) {
        if (input.length() == 0) {
            return -1; //No exception throwing
        }
        if (endIndex >= input.length() | endIndex < 0) {
            throw new IllegalArgumentException("Index was out from borders of string");
        }

        if (index >= input.length() | index < 0) {
            throw new IllegalArgumentException("Index was out from borders of string");
        }
        if (index >= endIndex) {
            throw new IllegalArgumentException("Index higher than endIndex, or equals it");
        }
        for (int i = index; i < endIndex; i++) {
            if (input.charAt(i) == req) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Возвращает заполненную символами {@code template} строку, длина которой равна параметру {@code length}.
     * @param length Длина генерируемой строки
     * @param template Символ, которым необходимо заполнить строку
     * @return Строка длиной {@code length}, содержимое которой представляет собой повторяющиеся символы {@code template}
     */
    public static String genFilledString(int length, char template) {
        StringBuffer result = new StringBuffer(); // Для экономии ресурсов
        for (int i = 0; i < length; i++) {
            result.append(template);
        }
        return result.toString();
    }
}